<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Department_model extends CI_Model {

		private $table = 'department';
		
		function __construct(){
			parent::__construct();
		}

		public function getAll(){

			$this->db->select("*");
			$this->db->from($this->table);
			return $this->db->get()->result();
		}

		public function get_by_organisasi($id_organisasi){
			$this->db->select("*");
			$this->db->from($this->table);
			$this->db->where("id_organisasi",$id_organisasi);
			return $this->db->get()->result();
		}

		public function add($data){
			$this->db->insert($this->table,$data);
			return ($this->db->affected_rows() != 1) ? false : true;
		}

		public function update($nama,$id){
			$this->db->set('name', $nama);
			$this->db->where('id', $id);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;	
		}

		public function delete($id){
			$this->db->where('id', $id);
			$this->db->delete($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;
		}

	}
?>