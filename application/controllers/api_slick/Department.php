<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller {

	function __construct(){
		/*header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');*/
		parent::__construct();

		$this->load->model('department_model');
	}

	public function getAll(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		$json = array (
						'message' 	=> 'List department berhasil diambil',
						'data'		=> $this->department_model->getAll(),
						'code'		=> 200

		);

		$this->outputJson($json);
	}

	public function get_department_by_organisasi(){
		$json = array (
			'message' 	=> 'List department berhasil diambil',
			'data'		=> $this->department_model->get_by_organisasi($this->input->post('id_organisasi')),
			'code'		=> 200
		);

		$this->outputJson($json);
	}

	public function add_department(){
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		log_message("error","organisasi".$this->input->post('id_organisasi'));
		@$nama = $this->input->post('nama');
		@$id_organisasi = $this->input->post('id_organisasi');

		$json = array (
			'message' 	=> 'Departemen berhasil ditambahkan',
			'data'		=> $this->department_model->add(array('name' =>$nama,'id_organisasi'=>$id_organisasi)),
			'code'		=> 200
		);

		$this->outputJson($json);
	}

	public function edit_department(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		$json = array (
						'message' 	=> 'Nama departemen berhasil diubah',
						'data'		=> $this->department_model->update($postdata['name'],$postdata['id']),
						'code'		=> 200

		);

		$this->outputJson($json);
	}

	public function delete_department(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		$id = (!empty($postdata['id'])) ? $postdata['id'] : 0;
		$json = array (
			'message' 	=> 'Departemen berhasil dihapus',
			'data'		=> $this->department_model->delete($id),
			'code'		=> 200

		);

		$this->outputJson($json);
	}

	private function outputJson($response=array(),$status=200){
		$this->output
		->set_status_header($status)
		->set_content_type('application/json', 'utf-8')
		->set_output(json_encode($response, JSON_PRETTY_PRINT))
		->_display();
		exit();
	}

}