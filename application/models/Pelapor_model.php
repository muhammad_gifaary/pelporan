<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Pelapor_model extends CI_Model {

		private $table = 'pelapor';
		
		function __construct(){
			parent::__construct();
			header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');
			header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');
		}

		public function add($data){
			$this->db->insert($this->table,$data);
			$insert_id = $this->db->insert_id();

   			return  $insert_id;
		}

		public function add_pelapor_organisasi($data){
			$this->db->insert("pelapor_organisasi",$data);
		}

		public function getPelaporById($id){
			$this->db->select('device_token');
			$this->db->from($this->table);
			$this->db->where('id',$id);

			return $this->db->get()->result();
		}
		public function get_id_organisasi($id){
			$this->db->select('username,email,no_telphone,id_organisasi');
			$this->db->from($this->table);
			$this->db->join("pelapor_organisasi","{$this->table}.id=pelapor_organisasi.id_pelapor");
			$this->db->where('id',$id);

			return $this->db->get()->row();
		}

		public function getDataPelaporByEmail($email){
			$this->db->select("username,email,no_telphone, id, password,device_token");
			$this->db->where($this->table.'.email',$email);
			$query = $this->db->get($this->table)->result();

			return $query;
		}
		public function getDataPelapor($id){
			$this->db->select('username,email,no_telphone,pelapor_organisasi.id_organisasi,nama as nama_organisasi');
			$this->db->join("pelapor_organisasi","pelapor_organisasi.id_pelapor={$this->table}.id");
			$this->db->join('organisasi', 'organisasi.id=pelapor_organisasi.id_organisasi');
			$this->db->where($this->table.'.id',$id);
			$query = $this->db->get($this->table)->result();

			return $query;
		}

		public function getDataPelaporByIdOrganisasi($id, $id_organisasi){
			$this->db->select('username,email,no_telphone,pelapor_organisasi.id_organisasi,nama as nama_organisasi');
			$this->db->join("pelapor_organisasi","pelapor_organisasi.id_pelapor={$this->table}.id");
			$this->db->join('organisasi', 'organisasi.id=pelapor_organisasi.id_organisasi');
			$this->db->where($this->table.'.id',$id);
			$this->db->where("pelapor_organisasi.id_organisasi",$id_organisasi);
			$query = $this->db->get($this->table)->result();

			return $query;
		}

		public function getlaporanByid($id,$limit,$offset){
			
			$this->db->select('laporan.*');
			$this->db->select("CONCAT('".IMAGE."',laporan.foto_pelapor) as foto_pelapor");
			$this->db->select("CONCAT('".IMAGE."',laporan.foto_solver) as foto_solver");
			$this->db->select("date_trunc('seconds',CASE laporan.status_urgent
								WHEN 1 THEN laporan.waktu_lapor + interval'1h'*12
								WHEN 2 THEN laporan.waktu_lapor + interval'1h'*24
								WHEN 3 THEN laporan.waktu_lapor + interval'1h'*3*24
								WHEN 4 THEN laporan.waktu_lapor + interval'1h'*7*24
							END::timestamp) as time_limit");
			$this->db->select("NULLIF(admin.username,'-') as nama_solver");
			$this->db->select("pelapor.username as nama_pelapor");

			$this->db->from('laporan');
			$this->db->join("pelapor","laporan.id_pelapor=pelapor.id");
			$this->db->join("admin","laporan.id_admin=admin.id","LEFT");
			$this->db->where('laporan.id_pelapor',$id);
			$this->db->order_by('laporan.waktu_lapor','DESC');
			$this->db->limit($limit,$offset);
			return $this->db->get()->result();
		}

		public function getOneLaporan($id){
			$this->db->select('laporan.*');
			$this->db->select("date_trunc('seconds',laporan.waktu_lapor) as waktu_lapor");
			$this->db->select('laporan.id as laporanId');
			$this->db->select("CONCAT('".IMAGE."',laporan.foto_pelapor) as foto_pelapor");
			$this->db->select("CONCAT('".IMAGE."',laporan.foto_pelapor2) as foto_pelapor2");
			$this->db->select("CONCAT('".IMAGE."',laporan.foto_pelapor3) as foto_pelapor3");
			$this->db->select("laporan.foto_pelapor2 as foto2");
			$this->db->select("laporan.foto_pelapor3 as foto3");		
			$this->db->select("CONCAT('".IMAGE."',laporan.foto_solver) as foto_solver");
			$this->db->select("CONCAT('".IMAGE."',laporan.foto_solver2) as foto_solver2");
			$this->db->select("CONCAT('".IMAGE."',laporan.foto_solver3) as foto_solver3");
			$this->db->select("laporan.foto_solver2 as fotos2");
			$this->db->select("laporan.foto_solver3 as fotos3");	

			$this->db->select("date_trunc('seconds',CASE laporan.status_urgent
								WHEN 1 THEN laporan.waktu_lapor + interval'1h'*12
								WHEN 2 THEN laporan.waktu_lapor + interval'1h'*24
								WHEN 3 THEN laporan.waktu_lapor + interval'1h'*3*24
								WHEN 4 THEN laporan.waktu_lapor + interval'1h'*7*24
							END::timestamp) as time_limit");
			$this->db->select("NULLIF(admin.username,'-') as nama_solver");
			$this->db->select("pelapor.username as nama_pelapor, department.name as depname");

			$this->db->from('laporan');
			$this->db->join('admin','admin.id=laporan.id_admin','left');
			$this->db->join("pelapor","laporan.id_pelapor=pelapor.id");
			$this->db->join('department','department.id=laporan.id_department');
			$this->db->where('laporan.id',$id);
			return $this->db->get()->result_array();
		}

		public function addLaporan($data=array()){
			$query = $this->db->set('waktu_lapor', 'NOW()');
			$query = $this->db->set('status_urgent', $data['status_urgent']);
			$query = $this->db->set('lokasi', $data['lokasi']);
			$query = $this->db->set('notes_pelapor', $data['notes_pelapor']);
			$query = $this->db->set('id_department', $data['id_department']);
			$query = $this->db->set('id_pelapor', $data['id_pelapor']);
			$query = $this->db->set('foto_pelapor', $data['foto_pelapor']);
			$query = $this->db->insert('laporan',$data);
			return $query;
		}

		public function getAll($offset=0,$limit=10){
			$this->db->select('id,email,status,no_telphone,username,device_token');
			$this->db->from($this->table);
			$this->db->where("status_delete",true);
			$this->db->order_by('status', 'DESC');
			$this->db->limit($limit,$offset);
			
			return $this->db->get()->result();
		}

		public function get_by_organisasi($offset=0, $limit=10, $id_organisasi=0){
			$this->db->select('id,email,status_active as status,no_telphone,username,device_token');
			$this->db->from($this->table);
			$this->db->join("pelapor_organisasi","{$this->table}.id=pelapor_organisasi.id_pelapor");
			$this->db->where("status_delete",true);
			$this->db->where("pelapor_organisasi.id_organisasi",$id_organisasi);
			$this->db->order_by('status', 'DESC');
			$this->db->limit($limit,$offset);
			
			return $this->db->get()->result();
		}

		public function cekEmail($email){
			$this->db->select('*');
			$this->db->from($this->table);
			$this->db->where('email',$email);

			$query = $this->db->get();
			$status = true;
			if($query->num_rows() > 0){
				$status = true;
			}else{
				$status = false;
			}

			return $status;
		}

		public function cek_active($email){
			$this->db->select('status_active');
			$this->db->from($this->table);
			$this->db->where('email',$email);

			$query = $this->db->get()->row();
			if($query!=null){
				return $query->status_active;	
			}else{
				return null;
			}
			
		}

		public function cekUsername($username){
			$this->db->select('*');
			$this->db->from($this->table);
			$this->db->where('username',$username);

			$query = $this->db->get();
			$status = true;
			if($query->num_rows() > 0){
				$status = true;
			}else{
				$status = false;
			}

			return $status;
		}

		public function cekPhone($phone){
			$this->db->select('*');
			$this->db->from($this->table);
			$this->db->where('no_telphone',$phone);

			$query = $this->db->get();
			$status = true;
			if($query->num_rows() > 0){
				$status = true;
			}else{
				$status = false;
			}
			
			return $status;
		}

		public function cekPassword($password){
			$this->db->select('*');
			$this->db->from($this->table);
			$this->db->where('password',$password);

			$query = $this->db->get();
			$status = true;
			if($query->num_rows() > 0){
				$status = true;
			}else{
				$status = false;
			}
			
			return $status;
		}

		public function cek_token($token){
			$this->db->select('token');
			$this->db->from($this->table);
			$this->db->where('token',$token);

			$query = $this->db->get();
			$status = true;
			if($query->num_rows() > 0){
				$status = true;
			}else{
				$status = false;
			}
			
			return $status;
		}

		public function cekLogin($data){
			$this->db->select('email,id,no_telphone,username,device_token,status,token');
			$this->db->from($this->table);
			$this->db->where('email',$data['email']);
			$this->db->where('password',$data['password']);
			$this->db->where('status',true);
			$this->db->where('status_delete',true);
			$this->db->where('status_active',true);

			$query = $this->db->get();
			/*$status = false;
			if($query->num_rows() > 0){
				$status = true;
			}else{
				$status = false;
			}*/

			return $query;
		}
		public function logout($email){
			$this->db->set('device_token', '');
			$this->db->where('email', $email);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;
		}
		public function updateStatusActive($id){

			$this->db->set('status', true);
			$this->db->where('id', $id);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;
		}

		public function update_status_by_admin_user($id){

			$this->db->set('status_active', true);
			$this->db->where('id', $id);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;
		}

		public function updatePassword($data){
			$this->db->set('password', $data['password']);
			$this->db->where('id', $data['id']);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;	
		}

		public function updatePasswordByEmail($data){
			$this->db->set('password', $data['password']);
			$this->db->where('email', $data['email']);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;	
		}

		public function updateDeviceToken($device_token,$email,$token){
			$this->db->set('device_token', $device_token);
			$this->db->set('token', $token);
			$this->db->where('email', $email);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;	
		}

		public function update($data,$email){

			$this->db->where('email', $email);
			$this->db->update($this->table, $data);
			return true;
		}

		public function delete_pelapor($id){
			$this->db->set('status_delete', false);
			$this->db->set('device_token', '');
			$this->db->set('no_telphone', '');
			$this->db->set('email', '');
			$this->db->where('id', $id);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;
		}

		public function delete_pelapor_by_id_organisasi($id){
			$this->db->set('status_delete', false);
			$this->db->set('device_token', '');
			$this->db->set('no_telphone', '');
			$this->db->set('email', '');
			$this->db->where('id_organisasi', $id);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;
		}

		public function get_list_organisasi_by_email($email){

			$this->db->select("organisasi.id,organisasi.nama");
			$this->db->from("pelapor");
			$this->db->join("pelapor_organisasi","pelapor.id=pelapor_organisasi.id_pelapor");
			$this->db->join("organisasi","organisasi.id=pelapor_organisasi.id_organisasi");
			$this->db->where('pelapor.email',$email);
			$this->db->where('organisasi.status',true);
			$this->db->group_by('organisasi.id');

			$query = $this->db->get()->result();

			$this->db->select("organisasi.id,organisasi.nama");
			$this->db->from("admin");
			$this->db->join("admin_organisasi","admin.id=admin_organisasi.id_admin");
			$this->db->join("organisasi","organisasi.id=admin_organisasi.id_organisasi");
			$this->db->where('admin.email',$email);
			$this->db->where('organisasi.status',true);
			$this->db->group_by('organisasi.id');
			$this->db->group_by('admin_organisasi.id_admin_group');

			$query2 = $this->db->get()->result();
			
			return array_merge($query,$query2);
		}

	}
?>