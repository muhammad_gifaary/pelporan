<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Admin_model extends CI_Model {

		private $table = 'admin';
		
		function __construct(){
			parent::__construct();
		}

		public function GetAdminById($id){

			$this->db->select("*");
			$this->db->from($this->table);
			$this->db->where("id",$id);
			return $this->db->get()->result();
		}

		public function logout($email){
			$this->db->set('device_token', '');
			$this->db->where('email', $email);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;
		}

		public function delete_admin($id){
			$this->db->set('status', false);
			$this->db->set('device_token', '');
			$this->db->set('no_telphone', '');
			$this->db->set('email', '');
			$this->db->where('id', $id);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;
		}

		public function delete_admin_by_id_orgnisasi($id){
			$this->db->set('status', false);
			$this->db->set('device_token', '');
			$this->db->set('no_telphone', '');
			$this->db->set('email', '');
			$this->db->where('id_organisasi', $id);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;
		}

		public function get_solver($offset,$limit,$id_organisasi){

			$this->db->select("id,email,no_telphone,username, status");
			$this->db->from($this->table);
			$this->db->join("admin_organisasi","{$this->table}.id=admin_organisasi.id_admin");
			$this->db->where("admin_organisasi.id_admin_group","1");
			$this->db->where("status",true);
			$this->db->where("admin_organisasi.id_organisasi",$id_organisasi);
			$this->db->limit($limit,$offset);
			return $this->db->get()->result();
		}

		public function get_id_organisasi($id){
			$this->db->select('admin_organisasi.id_organisasi');
			$this->db->from($this->table);
			$this->db->join("admin_organisasi","{$this->table}.id=admin_organisasi.id_admin");
			$this->db->where('id',$id);

			return $this->db->get()->row();
		}

		public function get_admin_user($offset,$limit,$id_organisasi){

			$this->db->select("id,email,no_telphone,username, status");
			$this->db->from($this->table);
			$this->db->join("admin_organisasi","{$this->table}.id=admin_organisasi.id_admin");
			$this->db->where("admin_organisasi.id_admin_group","2");
			$this->db->where("status",true);
			$this->db->where("admin_organisasi.id_organisasi",$id_organisasi);
			$this->db->limit($limit,$offset);
			return $this->db->get()->result();
		}

		public function getListAdminGroup(){
			$this->db->select('*');
			$this->db->from("admin_group");

			return $this->db->get()->result();
		}

		public function getDataAdmin($email){
			$query = $this->db->select('username,email,no_telphone,admin_organisasi.id_organisasi,nama as nama_organisasi, password, device_token');
			$query = $this->db->join("admin_organisasi","{$this->table}.id=admin_organisasi.id_admin");
			$query = $this->db->join('organisasi', 'organisasi.id=admin_organisasi.id_organisasi');
			$query = $this->db->where($this->table.'.email',$email);
			$query = $this->db->get($this->table)->result();
			return $query;
			
		}

		public function getDataAdminByIdAndOrganisasi($id,$id_organisasi){
			$query = $this->db->select('admin.id');
			$query = $this->db->join("admin_organisasi","{$this->table}.id=admin_organisasi.id_admin");
			$query = $this->db->join('organisasi', 'organisasi.id=admin_organisasi.id_organisasi');
			$query = $this->db->where($this->table.'.id',$id);
			$query = $this->db->where("admin_organisasi.id_organisasi",$id_organisasi);
			$query = $this->db->get($this->table)->result();
			return $query;
			
		}

		public function cekUsername($username){
			$this->db->select('*');
			$this->db->from($this->table);
			$this->db->where('username',$username);

			$query = $this->db->get();
			$status = true;
			if($query->num_rows() > 0){
				$status = true;
			}else{
				$status = false;
			}

			return $status;
		}

		public function cekEmail($email){
			$this->db->select('*');
			$this->db->from($this->table);
			$this->db->where('email',$email);

			$query = $this->db->get();
			$status = true;
			if($query->num_rows() > 0){
				$status = true;
			}else{
				$status = false;
			}

			return $status;
		}

		public function cekPhone($phone){
			$this->db->select('*');
			$this->db->from($this->table);
			$this->db->where('no_telphone',$phone);

			$query = $this->db->get();
			$status = true;
			if($query->num_rows() > 0){
				$status = true;
			}else{
				$status = false;
			}
			
			return $status;
		}

		public function cekPassword($password){
			$this->db->select('*');
			$this->db->from($this->table);
			$this->db->where('password',$password);

			$query = $this->db->get();
			$status = true;
			if($query->num_rows() > 0){
				$status = true;
			}else{
				$status = false;
			}
			
			return $status;
		}

		public function add($data){
			$this->db->insert($this->table,$data);
			return $this->db->insert_id();
		}

		public function add_admin_organisasi($data){
			$this->db->insert("admin_organisasi",$data);
			return ($this->db->affected_rows() != 1) ? false : true;
		}

		public function updatePassword($data){
			$this->db->set('password', $data['password']);
			$this->db->where('id', $data['id']);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;	
		}

		public function updatePasswordByEmail($data){
			$this->db->set('password', $data['password']);
			$this->db->where('email', $data['email']);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;	
		}

		public function updateDeviceToken($device_token,$email,$token){
			$this->db->set('device_token', $device_token);
			$this->db->set('token', $token);
			$this->db->where('email', $email);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;	
		}

		public function updateStatusActive($id){

			$this->db->set('status_active', true);
			$this->db->where('id', $id);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;
		}

		public function cek_token($token){
			$this->db->select('token');
			$this->db->from($this->table);
			$this->db->where('token',$token);

			$query = $this->db->get();
			$status = true;
			if($query->num_rows() > 0){
				$status = true;
			}else{
				$status = false;
			}
			
			return $status;
		}

		public function cekLogin($data){
			$this->db->select('email,no_telphone,username,admin_organisasi.id_department, admin_organisasi.id_admin_group,name, admin_organisasi.id_organisasi,token');
			$this->db->select('admin.id as id');
			$this->db->from($this->table);
			$this->db->join("admin_organisasi","{$this->table}.id=admin_organisasi.id_admin");
			$this->db->join("admin_group","admin_group.id=admin_organisasi.id_admin_group");
			$this->db->where('email',$data['email']);
			$this->db->where('password',$data['password']);
			$this->db->where('status',true);

			$query = $this->db->get();
			/*$status = false;
			if($query->num_rows() > 0){
				$status = true;
			}else{
				$status = false;
			}*/

			return $query;
		}

		public function cek_active($email){
			$this->db->select('status_active');
			$this->db->from($this->table);
			$this->db->where('email',$email);

			$query = $this->db->get()->row();
			if($query!=null){
				return $query->status_active;	
			}else{
				return null;
			}
			
		}

		public function GetAdminByDepartment($id){

			$this->db->select("device_token");
			$this->db->from($this->table);
			$this->db->join("admin_organisasi","{$this->table}.id=admin_organisasi.id_admin");
			$this->db->where("admin_organisasi.id_department",$id);
			return $this->db->get()->result();
		}

		public function GetAdminByDepartmentAndOrganisasi($id_department,$id_organisasi){

			$this->db->select("device_token");
			$this->db->from($this->table);
			$this->db->join("admin_organisasi","{$this->table}.id=admin_organisasi.id_admin");
			$this->db->where("admin_organisasi.id_department",$id_department);
			$this->db->where("admin_organisasi.id_organisasi",$id_organisasi);
			return $this->db->get()->result();
		}

		public function update($data,$email){

			$this->db->where('email', $email);
			$this->db->update($this->table, $data);

			return true;
		}

		public function get_role($email,$id_organisasi){
			$this->db->select("admin_organisasi.id_admin_group,admin_organisasi.id_department, admin.id");
			$this->db->from("admin");
			$this->db->join("admin_organisasi","admin.id=admin_organisasi.id_admin");
			$this->db->where("admin.email",$email);
			$this->db->where("admin_organisasi.id_organisasi",$id_organisasi);
			$query = $this->db->get()->result();
			return $query;
		}

	}
?>