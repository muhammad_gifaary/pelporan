<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function register(){
		$this->load->model('admin_model');
		$this->load->model('organisasi_model');
		if(!$this->admin_model->cekUsername($this->input->post('username'))){
			if(!$this->admin_model->cekEmail($this->input->post('email'))){
					$id_organisasi = $this->organisasi_model->add(array("nama"=>$this->input->post('organisasi')));
					//insert admin
					$data['email']=$this->input->post('email');
					$data['no_telphone']=$this->input->post('phone');
					$data['username']=$this->input->post('username');
					$data['password']=md5($this->input->post('password'));
					$data['status_active']=false;
					$id_admin = $this->admin_model->add($data);
					//insert to admin organisasi

					$data_admin_organisasi['id_department']=0;
					$data_admin_organisasi['id_organisasi']=$id_organisasi;
					$data_admin_organisasi['id_admin']=$id_admin;
					$data_admin_organisasi['id_admin_group']=2;
					$this->admin_model->add_admin_organisasi($data_admin_organisasi);
					
					$this->email->from('info@adhmora.com', "Admin Slick");
					$this->email->to($this->input->post('email'));

					$this->email->subject('Aktivasi Akun');
					$data['id_admin']=$id_admin;
					$pesan = $this->load->view("template-email-register",$data,TRUE);
					$this->email->message($pesan);
					$this->email->set_mailtype('html');
					if (!$this->email->send()) {
				             log_message('error',$this->email->print_debugger());
				    }
				    $data =  array(
						"code" => "200",
						"message"	=> "Succes register"
					); 
			}else{
				$data =  array(
					"code" => "500",
					"message"	=> "Email already used"
				);
			}
		}else{
			$data =  array(
				"code" => "500",
				"message"	=> "Username already used"
			);
		}
		print_r(json_encode($data));
	}

	public function sent_email(){
		//$this->email->initialize($data);
		$this->email->from('info@adhmora.com', "Admin Slick");
		$this->email->to("m121524018@students.jtk.polban.ac.id");

		$this->email->subject('Aktivasi Akun');
		$data['id_admin']=1;
		$message = $this->load->view("template-email-register",$data,TRUE);
		$this->email->message($message);
		$this->email->set_mailtype('html');
		if (!$this->email->send()) {
             log_message('error',$this->email->print_debugger());
        } 
	}

	public function active_account($id_admin){
		$this->load->model('admin_model');
		$id = base64_decode($id_admin);
		$status = $this->admin_model->updateStatusActive($id);
		if($status){
			echo "<font size='6'><b>Akun telah diaktifkan, Silahkan login pada aplikasi</b></font>";	
		}else{
			echo "<font size='6'><b>Sory, Your link not valid.</b></font>";
		}
	}

	public function sign_up_invitation($id_pelapor,$id_organisasi,$id_department=0){
		$this->load->model("admin_model");
		$this->load->model("pelapor_model");
		if(base64_decode($id_department) > 0){ //as solver
			$email = $this->admin_model->GetAdminById(base64_decode($id_pelapor));
			//log_message("error",print_r($email));
		}else{ // as pelapor
			$email = $this->pelapor_model->getDataPelapor(base64_decode($id_pelapor));
		}
		if(empty($email)){
			redirect('welcome');
		}else{
			$data['id_pelapor'] = base64_decode($id_pelapor);
			$data['id_organisasi'] = base64_decode($id_organisasi);
			$data['id_department'] = base64_decode($id_department);
			$data['email'] = $email[0]->email;
			$data['username'] = '';
    		$data['password'] = '';
    		$data['no_telphone'] = '';
			$this->load->view('sign_up',$data);
		}
		
	}

	public function register_invitation(){
		$this->load->library('form_validation');
		$this->load->model("admin_model");
		$this->load->model("pelapor_model");
		
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('no_telphone', 'No telphone', 'required');
        $data = array();
        $data['id_pelapor'] = $this->input->post("id_pelapor");
        $data['username'] = $this->input->post("username");
		$data['id_organisasi'] = $this->input->post("id_organisasi");
		$data['id_department'] = $this->input->post("id_department");
		$data['no_telphone'] = $this->input->post("no_telphone");
		$data['email']		= $this->input->post('email');
		$data['password']	= md5($this->input->post('password'));
        if ($this->form_validation->run() == FALSE)
        {
           	$this->load->view('sign_up',$data);
        }
        else
        {
        	if($data['id_department'] > 0){
        		//update data admin
        		$data_admin['username'] = $data['username'];
        		$data_admin['password'] = $data['password'];
        		$data_admin['no_telphone'] = $data['no_telphone'];
        		$data_admin['status_active'] = true;
        		$this->admin_model->update($data_admin,$data['email']);
        	}else{
        		//update data pelapor
        		$data_admin['username'] = $data['username'];
        		$data_admin['password'] = $data['password'];
        		$data_admin['no_telphone'] = $data['no_telphone'];
        		$data_admin['status_active'] = true;
        		$this->pelapor_model->update($data_admin,$data['email']);
        	}
        	$this->session->set_flashdata('msg', 'success, Now you can login in slick');
           	redirect('welcome');
        }
	}
}
