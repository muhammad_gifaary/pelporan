<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organisasi extends CI_Controller {

	function __construct(){
		/*header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');*/
		parent::__construct();

		$this->load->model('organisasi_model');
		$this->load->model('admin_model');
		$this->load->model('pelapor_model');
		$this->load->model('department_model');
	}

	public function getAll(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		$json = array (
						'message' 	=> 'List organisasi berhasil diambil',
						'data'		=> $this->organisasi_model->getAll(),
						'code'		=> 200

		);

		$this->outputJson($json);
	}

	public function get_list_organisasi_by_email(){
		$postdata = (array)json_decode(file_get_contents('php://input'));

		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		$email = $this->input->post("email");
		$list_organisasi = $this->pelapor_model->get_list_organisasi_by_email($email);
		$json = array (
					'message' 	=> 'List organisasi berhasil diambil',
					'data'		=> $list_organisasi,
					'code'		=> 200
		);
		$this->outputJson($json);
	}


	public function add_organisasi(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		$json = array (
						'message' 	=> 'Organisasi berhasil ditambahkan',
						'data'		=> $this->organisasi_model->add(array('nama' =>$postdata['nama'])),
						'code'		=> 200

		);

		$this->outputJson($json);
	}

	public function edit_organisasi(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		$json = array (
			'message' 	=> 'Nama organisasi berhasil diubah',
			'data'		=> $this->organisasi_model->update($postdata['nama'],$postdata['id']),
			'code'		=> 200

		);

		$this->outputJson($json);
	}

	public function delete_organisasi(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		$id = (!empty($postdata['id'])) ? $postdata['id'] : 0;
		$json = array (
						'message' 	=> 'Organisasi berhasil dihapus',
						'data'		=> $this->organisasi_model->delete($id),
						'code'		=> 200

		);

		$this->outputJson($json);
	}

	public function get_organisasi_by_id(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		$id = $postdata['id_organisasi'];
		$json = array (
			'message' 	=> 'Organisasi berhasil diambil',
			'data'		=> $this->organisasi_model->get_by_id($id),
			'code'		=> 200

		);

		$this->outputJson($json);
	}

	private function outputJson($response=array(),$status=200){
		$this->output
		->set_status_header($status)
		->set_content_type('application/json', 'utf-8')
		->set_output(json_encode($response, JSON_PRETTY_PRINT))
		->_display();
		exit();
	}

}