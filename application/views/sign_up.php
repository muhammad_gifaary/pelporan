<html  lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Slick - Application</title>

    <link href="<?php echo ASSETS_URI;?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ASSETS_URI;?>css/full.css" rel="stylesheet">

    
    
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet"> 
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">

</head>

<body class="full">

    <div class="container">
        <div class="row"> 
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header">
                <div class="col-lg-5 col-md-4 col-sm-12 col-xs-12 menu visible-lg visible-md">
                   <!--  <button class="btn btn-login"><span class=" glyphicon glyphicon-th-large"></span></button> -->
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 logo ">
                    <a href="#"><img class="img-responsive" src="<?php echo ASSETS_URI;?>img/logo.png"></a>
                </div>
                 <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 menu">
                    <ul>
                        <li class="btn-one"><button class="btn btn-login" data-target="#myModal" data-toggle="modal"><span class=" glyphicon glyphicon-user"></span>     Sign In</button></li>
                        <li><button class="btn btn-sign-in" data-target="#signup" data-toggle="modal"><span class="glyphicon glyphicon-edit"></span> Sign Up</button></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal">
        <!---- penting ------>
        <div class="modal-dialog">
                <div class="loginmodal-container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <h1>Login to Your Account</h1><br>
                    </div>
                    
                  <form>
                    <input type="text" name="user" placeholder="Username">
                    <input type="password" name="pass" placeholder="Password">
                    <input type="submit" name="login" class="login loginmodal-submit" value="Sign In">
                  </form>
                    
                  <div class="login-help">
                    <a href="#">Forgot Password</a>
                  </div>
                </div>
        </div>
    </div>

    <div class="modal fade" id="signup">
        <!---- penting ------>
        <div class="modal-dialog">
                <div class="loginmodal-container">
                	<div class="alert alert-success alert-dismissable" id="alert">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Success!</strong> Please check your email for verification.
					</div>
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                   <h1>Sign Up As a Admin</h1><br>
	                </div>
                <form id="register" class="register" name="register">
                  	<div class="error" id="error-email">
                    	Email is required
                    </div>
                    <input type="email" name="email" id="email" placeholder="E-Mail" class="form-control" required>
                    <div class="error" id="error-username">
                    	Username is required
                    </div>
                    <input type="text" name="username" id="username" placeholder="Username" class="form-control" required>
                    <div class="error" id="error-phone">
                    	Phone number character range in 9-13
                    </div>
                    <input type="text" name="phone" id="phone" placeholder="Phone Number" class="form-control numeric" required>
                    <div class="error" id="error-organization">
                    	Organization name is required
                    </div>
                    <input type="text" name="organisasi" id="organisasi" placeholder="Organization Name" class="form-control" required>
                    <div class="error" id="error-password">
                     	Password is required
                    </div>
                    <input type="password" name="password" id="password" placeholder="Password" class="form-control" required>
                    <div class="error" id="error-retype-password">
                     	Password didn't match
                    </div>
                    <input type="password" name="retype_password" id="retype_password" placeholder="Retype Password" class="form-control" required>
                    <input type="submit" id="sign_up" name="login" class="login loginmodal-submit" value="Sign Up">
                </form>
                </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 content"> 
                <div class="col-lg-6 image pull-right"> 
                    <img class="img-responsive" src="<?php echo ASSETS_URI;?>img/object.png" style="max-width: 90%;">
                </div>
                <div class="col-lg-6 warp-content-data" style="padding-bottom: none;">
                    <div class="col-lg-12 visible-lg"> 
                        &nbsp;
                    </div> 
                    <div class="col-lg-12 header-content" style="text-align: center"> 
                        <h1><b>Register</b></h1>
                    </div>
                    <div class="col-lg-12 content-data" style="text-align: left">
                        
                        <div class="alert alert-error alert-dismissable" id="alert">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <?php echo validation_errors(); ?> 
                        </div>
                        <form method="post" action="<?php echo site_url('welcome/register_invitation')?>">
                            <input type="email" name="id_pelapor" id="id_pelapor" class="form-control hidden" readonly="true" value="<?php echo $id_pelapor;?>">
                            <input type="email" name="id_organisasi" id="id_pelapor" class="form-control hidden" readonly="true" value="<?php echo $id_organisasi;?>">
                            <input type="email" name="id_department" id="id_pelapor" class="form-control hidden" readonly="true" value="<?php echo $id_department;?>">
                            <div class="form-group">
                                <input type="email" name="email" id="email" placeholder="E-Mail" class="form-control" readonly="true" value="<?php echo $email;?>">
                            </div>
                            <div class="form-group">
                                <input type="text" name="username" id="username" placeholder="Username" class="form-control" value="<?php echo $username;?>" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="no_telphone" id="no_telphone" placeholder="Phone Number" class="form-control numeric" required value="<?php echo $no_telphone; ?>">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" id="password" placeholder="Password" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <input type="password" name="passconf" id="passconf" placeholder="Retype Password" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Sign Up</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 footer" style="margin-top: -80px;">
            <hr class="effect-border-style">
            <p class="content-footer" align="center">Powered By muhammadgifary@gmail.com Bandung &copy; 2017 - <b>© PT. Adhmora</b> </p>
        </div>
    </div>
</body>
</html>
<!-- js -->
<script src="<?php echo ASSETS_URI;?>js/jquery.js"></script>
<script src="<?php echo ASSETS_URI;?>js/bootstrap.min.js"></script>
<script src="<?php echo ASSETS_URI;?>js/custom.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(".error").hide();
    $("#alert").hide();
    <?php if($this->session->flashdata('msg')){ ?>
        alert('<?php echo $this->session->flashdata('msg'); ?>');
    <?php } ?>
});
$(".register").submit(function(e){
    e.preventDefault();
});

$("#sign_up").on("click",function(e){
    //e.preventdefault();
    e.preventDefault();
    console.log("click button");
    $(".error").hide();
    var email = $("#email").val();
    console.log(email);
    var username = $("#username").val()
    var phone = $("#phone").val()
    var organisasi = $("#organisasi").val()
    var password = $("#password").val()
    var retype_password = $("#retype_password").val()

    if(email!=''){
        if(username!=''){
            console.log(phone.length);
            if(phone.length > 9 && phone.length < 13){
                if(organisasi!=''){
                    if(password!=''){
                        if(retype_password==password){
                            var data = $("#register").serialize();
                            $.ajax({
                              method: "POST",
                              url: "<?php echo base_url('welcome/register');?>",
                              data: data
                            }).done(function( msg ) {
                                if(msg.code==500){
                                    alert("Sory "+msg.message);
                                }else{
                                    $("#alert").show();
                                    $("#register").trigger('reset');
                                }
                            });
                        }else{
                            $("#error-retype-password").show();
                        }
                    }else{
                        $("#error-password").show();
                    }
                }else{
                    $("#error-organization").show();
                }
            }else{
                $("#error-phone").show();
            }
        }else{
            $("#error-username").show();
        }
    }else{
        $("#error-email").show();
    }
});
</script>
