<?php
// API access key from Google API's Console
define( 'API_ACCESS_KEY', 'AIzaSyAQ3TZrGQadY4h3chJ7STthkWCfycxGn3w' );
$registrationIds = array( 'eSVvQ0UaYTc:APA91bG9US8cM57p9tjqDVn_gbSlbhP0AjjDFjQNTywvpD9-cq4yEsHW3zrKF5ialaE-Jgrec57AXg_gJ_DnEUorWDw-6OFnpCh6vHCt4z8zDD_RQMV0dV1JXnA7SDkycRxEqhSLeA0F' );
// prep the bundle
$msg = array
(
	'message' 	=> 'here is a message. message',
	'title'		=> 'This is a title. title',
	'subtitle'	=> 'This is a subtitle. subtitle',
	'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
	'vibrate'	=> 1,
	'sound'		=> 1,
	'largeIcon'	=> 'large_icon',
	'smallIcon'	=> 'small_icon'
);
$fields = array
(
	'registration_ids' 	=> $registrationIds,
	'data'			=> $msg
);
 
$headers = array
(
	'Authorization: key=' . API_ACCESS_KEY,
	'Content-Type: application/json'
);
 
$ch = curl_init();
curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
curl_setopt( $ch,CURLOPT_POST, true );
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
$result = curl_exec($ch );
curl_close( $ch );
echo $result;