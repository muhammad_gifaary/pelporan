<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Laporan_model extends CI_Model {

		private $table = 'laporan';
		
		function __construct(){
			parent::__construct();
		}

		public function add($data,$gambar){

			$this->db->set('waktu_lapor', 'NOW()');
			$this->db->set('status_urgent', $data['status_urgent']);
			$this->db->set('lokasi', $data['lokasi']);
			$this->db->set('id_department', $data['id_department']);
			$this->db->set('id_pelapor', $data['id_pelapor']);
			$this->db->set('foto_pelapor', $gambar);
			$this->db->insert($this->table);
			return ($this->db->affected_rows() != 1) ? false : true;
			
		}

		public function getLaporan($data,$limit,$offset){

			$this->db->select("{$this->table}.*");
			$this->db->select("NULLIF(admin.username,'-') as nama_solver");
			$this->db->select("pelapor.username as nama_pelapor");
			$this->db->select("CONCAT('".IMAGE."',{$this->table}.foto_pelapor) as foto_pelapor");
			$this->db->select("CONCAT('".IMAGE."',{$this->table}.foto_solver) as foto_solver");
			$this->db->select("date_trunc('seconds',CASE {$this->table}.status_urgent
								WHEN 1 THEN {$this->table}.waktu_lapor + interval'1h'*12
								WHEN 2 THEN {$this->table}.waktu_lapor + interval'1h'*24
								WHEN 3 THEN {$this->table}.waktu_lapor + interval'1h'*3*24
								WHEN 4 THEN {$this->table}.waktu_lapor + interval'1h'*7*24
							END::timestamp) as time_limit");
			$this->db->from($this->table);
			$this->db->join("pelapor","{$this->table}.id_pelapor=pelapor.id");
			$this->db->join("pelapor_organisasi","pelapor_organisasi.id_pelapor=pelapor.id");

			if($data['status_taken']==0 || $data['status_taken']==2){
				$this->db->join("admin","{$this->table}.id_admin=admin.id","LEFT");
				$this->db->where("{$this->table}.id_admin", '0');
			}elseif($data['status_taken']==3){
				$this->db->join("admin","{$this->table}.id_admin=admin.id");
				$this->db->where("{$this->table}.id_admin !=", '0');
			}else{
				$this->db->join("admin","{$this->table}.id_admin=admin.id","LEFT");
				$this->db->where("{$this->table}.id_admin", $data['id_admin']);
			}

			if(!empty($data['id_department'])){
				$this->db->where("{$this->table}.id_department", $data['id_department']);
			}
			if(!empty($data['id_organisasi'])){
				$this->db->where("pelapor_organisasi.id_organisasi", $data['id_organisasi']);
			}
			$this->db->order_by("{$this->table}.waktu_lapor",'DESC');
			$this->db->limit($limit,$offset);
			return $this->db->get()->result_array();

			//dapatkan data admin beserta role dari admin tersebut

		}

		public function getlaporanByid($id){
			$this->db->select("CONCAT('".IMAGE."',{$this->table}.foto_pelapor) as foto_pelapor");
			$this->db->select("CONCAT('".IMAGE."',{$this->table}.foto_solver) as foto_solver");
			$this->db->select("{$this->table}.waktu_lapor,{$this->table}.progress,{$this->table}.status_urgent,{$this->table}.lokasi,{$this->table}.notes_pelapor, {$this->table}.id_pelapor");
			$this->db->select("date_trunc('seconds',CASE {$this->table}.status_urgent
								WHEN 1 THEN {$this->table}.waktu_lapor + interval'1h'*12
								WHEN 2 THEN {$this->table}.waktu_lapor + interval'1h'*24
								WHEN 3 THEN {$this->table}.waktu_lapor + interval'1h'*3*24
								WHEN 4 THEN {$this->table}.waktu_lapor + interval'1h'*7*24
							END::timestamp) as time_limit");
			$this->db->select("NULLIF(admin.username,' ') as nama_solver");
			$this->db->select("pelapor.username as nama_pelapor");
			$this->db->from($this->table);
			$this->db->join("pelapor","{$this->table}.id_pelapor=pelapor.id");
			$this->db->join("admin","{$this->table}.id_admin=admin.id","LEFT");
			$this->db->where("{$this->table}.id",$id);

			return $this->db->get()->result();
		}

		public function updateProggress($data,$gambar1,$gambar2,$gambar3){
			$query = $this->db->set('progress', $data['progress']);
			$query = $this->db->set('foto_solver', $gambar1);
			$query = $this->db->set('foto_solver2', $gambar2);
			$query = $this->db->set('foto_solver3', $gambar3);
			$query = $this->db->set('notes', $data['notes']);
			$query = $this->db->where('id', $data['id']);
			$query = $this->db->update($this->table);

			return $query;
		}

		public function takenLaporan($data){
			$this->db->set('id_admin', $data['id_admin']);
			$this->db->where('id', $data['id_laporan']);
			$this->db->update($this->table);
		}

	}
?>