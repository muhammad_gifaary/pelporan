<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


	function __construct(){
		/*header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');*/
		parent::__construct();
		$this->load->library('image_lib');
		$this->load->model(array('admin_model','laporan_model','pelapor_model'));

	}

	public function index(){
		$this->load->view('welcome_message');
	}

	public function delete_admin(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		$status = $data = $this->admin_model->delete_admin($postdata['id']);
		if($status){
			$json = array (
					'message' 	=> 'Akun berhasil dihapus',
					'data'		=> $data,
					'code'		=> 200
			);
		}else{
			$json = array (
					'message' 	=> 'Data gagal dihapus',
					'data'		=> $data,
					'code'		=> 500
			);
		}
		$this->outputJson($json);
	}

	public function tes_list_solver(){
		$data = $this->admin_model->get_solver(0,10);
		print_r($data);

	}
	public function list_solver(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		@$offset = ($postdata['offset']!=null) ? $postdata['offset'] : 0;
		@$limit = ($postdata['limit']!=null) ? $postdata['limit'] : 10;

		$data = $this->admin_model->get_solver($offset,$limit,$postdata['id_organisasi']);
		if(!empty($data)){
			$json = array (
				'message' 	=> 'List solver berhasil di ambil',
				'data'		=> $data,
				'code'		=> 200
			);
		}else{
			$json = array (
				'message' 	=> 'List admin user kosong',
				'data'		=> '',
				'code'		=> 500
			);
		}
		$this->outputJson($json);
	}

	public function list_admin_user(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		@$offset = ($postdata['offset']!=null) ? $postdata['offset'] : 0;
		@$limit = ($postdata['limit']!=null) ? $postdata['limit'] : 10;

		$data = $this->admin_model->get_admin_user($offset,$limit,$postdata['id_organisasi']);
		if(!empty($data)){
			$json = array (
				'message' 	=> 'List admin user berhasil di ambil',
				'data'		=> $data,
				'code'		=> 200
			);
		}else{
			$json = array (
				'message' 	=> 'List admin user kosong',
				'data'		=> '',
				'code'		=> 500
			);
		}
		$this->outputJson($json);
	}

	public function edit_profile(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		@$no_telphone = $postdata['no_telphone'];
		@$username = $postdata['username'];
		@$email = $postdata['email'];

		// validasi
		$cekUsername = $this->admin_model->cekUsername($username);
		$cekPhone = $this->admin_model->cekPhone($no_telphone);
		$data_lama = $this->admin_model->getDataAdmin($email);

		if(!$cekUsername || strcmp($username, $data_lama[0]->username)==0){
			if($cekPhone==false || strcmp($no_telphone, $data_lama[0]->no_telphone)==0){
				$data = array(
					'no_telphone' => $no_telphone,
					'username'	=> $username
				);
				// insert data user
				$insert = $this->admin_model->update($data,$email);
				//$this->pelapor_model->update($data,$email);
				
				if($insert){
					$json = array (
						'message' 	=> 'Data pribadi berhasil diubah',
						'data'		=> '',
						'code'		=> 200
					);
				}else{
					$json = array (
						'message' 	=> 'Maaf anda gagal mengubah data pribadi ',
						'data'		=> '',
						'code'		=> 500
					);
				}
			}else{
				$json = array (
					'message' 	=> 'Maaf nomor telphone sudah digunakan',
					'data'		=> '',
					'code'		=> 500
				);
			}
		}else{
			$json = array (
				'message' 	=> 'Maaf username sudah digunakan',
				'data'		=> '',
				'code'		=> 500
			);
		}

		$this->outputJson($json);
	}

	public function get_data_admin(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		$data = $this->admin_model->getDataAdmin($postdata['email']);
		$json = array (
					'message' 	=> 'data berhasil di ambil',
					'data'		=> $data,
					'code'		=> 200
		);
		$this->outputJson($json);
	}

	public function uploadFile(){
		$location = trim($_POST['folder']);

		$uploadfile = $_POST['namaFile'];
		$uploadfilename = $_FILES['file']['tmp_name'];

		if(move_uploaded_file($uploadfilename, './'.$location.'/'.$uploadfile)){
			$config['image_library'] = 'gd2';
			$config['source_image']  = './uploads/'.$uploadfile;
			$config['new_image']  = './uploads/'.$uploadfile;
			$config['maintain_ratio'] = TRUE;
			$config['width']	 =400;
			$config['height']	= 400;

			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
			echo true;
		} else {
			echo false;
		}
	}
	
	public function sign_up(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		
		$data = array();
		$data['email']=$postdata['email'];
		$data['no_telphone']=$postdata['no_telphone'];
		$data['username']=$postdata['username'];
		$data['password']=md5($postdata['password']);
		$data['id_admin_group']=$postdata['id_admin_group'];
		$data['status_active']=true;

		if(!$this->admin_model->cekUsername($data['username'])){
			if(!$this->admin_model->cekEmail($data['email'])){
				if(!$this->admin_model->cekPhone($data['no_telphone'])){

					/*if(!empty($id_admin)){
						$id_organisasi = $this->admin_model->get_id_organisasi($id_admin);
						$data['id_organisasi'] = $id_organisasi->id_organisasi;	
					}else{
						$data['id_organisasi'] = $postdata['id_organisasi'];
					}*/
					
					$id_admin = $this->admin_model->add($data);
					if($id_admin > 0 ){
						$data_admin_organisasi['id_department']=$postdata['id_department'];
						$data_admin_organisasi['id_organisasi']=$postdata['id_organisasi'];
						$data_admin_organisasi['id_admin']=$id_admin;
						$this->admin_model->add_admin_organisasi($data_admin_organisasi);

						//berhasil infut
						$json = array (
							'message' 	=> 'Akun berhasil di daftarkan.',
							'data'		=> '',
							'code'		=> 200

						);
					}else{ //gagal input
						$json = array (
							'message' 	=> 'Maaf anda gagal mendaftar, silahkan coba lagi ',
							'data'		=> '',
							'code'		=> 500

						);
					}
				}else{ //no telphone sudah terdaftar
					$json = array (
							'message' 	=> 'Maaf nomor telphone sudah digunakan',
							'data'		=> '',
							'code'		=> 500

					);
				}
			}else{ //email sudah terdaftar
					$json = array (
							'message' 	=> 'Maaf email sudah digunakan',
							'data'		=> '',
							'code'		=> 500

					);
			}
		}else{ //email sudah terdaftar
					$json = array (
							'message' 	=> 'Maaf username sudah digunakan',
							'data'		=> '',
							'code'		=> 500

					);
		}

		$this->outputJson($json);
	}

	public function login(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		$data = array();
		$data['device_token']=$postdata ['device_token'];
		$data['email']=$postdata ['email'];
		$data['password']=md5($postdata ['password']);

		$status = $this->admin_model->cekLogin($data)->result();
		if($status!=null){
			//update device token
			$this->admin_model->updateDeviceToken($data['device_token'],$status[0]->id);
			$json = array (
						'message' 	=> 'Login Berhasil',
						'data'		=> $status,
						'code'		=> 200

			);
		}else{
			$json = array (
						'message' 	=> 'Email atau Password tidak sama',
						'data'		=> '',
						'code'		=> 500

			);
		}

		
		$this->outputJson($json);
	}

	public function reportProgress(){
		
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		if(!empty($postdata)){
			@$foto_solver1 = $postdata['foto_solver1'];
			@$foto_solver2 = $postdata['foto_solver2'];
			@$foto_solver3 = $postdata['foto_solver3'];

			$update = $this->laporan_model->updateProggress($postdata,$foto_solver1,$foto_solver2,$foto_solver3);


			if($update){
				//siap untuk report proggres ke pelapor
				if($postdata['progress']==100){
					$id_pelapor = $this->laporan_model->getlaporanByid($postdata['id']);
					foreach ($id_pelapor as $id_pelapor) {
						$device_token = $this->pelapor_model->getPelaporById($id_pelapor->id_pelapor);
						foreach ($device_token as $device_token) {
							$this->send_push_notif("Laporan telah selesai ditangani",$device_token->device_token);
						}
						
					}
				}
				$json = array (
					'message' 	=> 'Berhasil Lapor Progress',
					'data'		=> '',
					'code'		=> 200
				);
			}else{
				$json = array (
					'message' 	=> 'Gagal melaporkan',
					'data'		=> '',
					'code'		=> 500
				);
			}			
		}
        
		$this->outputJson($json);
	}

	public function tes($id){
		$admin = $this->admin_model->GetAdminByDepartment($id);
			foreach ($admin as $admin) {
				echo $admin->device_token;
			}
	}

	public function updatePassword(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		$data = array();
		$data['password'] = md5($postdata['password']);
		$data['email'] = $postdata['email'];
		@$password_lama = $postdata['password_lama'];

		if($this->admin_model->cekPassword(md5($password_lama))){
			if($this->admin_model->updatePasswordByEmail($data)){
				$this->pelapor_model->updatePasswordByEmail($data);
				$json = array (
							'message' 	=> 'Password berhasil diubah',
							'data'		=> '',
							'code'		=> 200

				);
			}else{
				$json = array (
							'message' 	=> 'Password gagal diubah',
							'data'		=> '',
							'code'		=> 500

				);
			}
		}else{
			$json = array (
							'message' 	=> 'Password lama salah',
							'data'		=> '',
							'code'		=> 500

			);
		}
		
		
		$this->outputJson($json);
	}

	public function listLaporan(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		@$offset = ($postdata['offset']!=null) ? $postdata['offset'] : 0;
		@$limit = ($postdata['limit']!=null) ? $postdata['limit'] : 10;
		log_message("error","status taken".$postdata['status_taken']);
		$data = $this->laporan_model->getLaporan($postdata,$limit,$offset);

		if(!empty($data)){
			$json = array (
				'message' 	=> 'List Laporan berhasil di ambil',
				'data'		=> $data,
				'code'		=> 200
			);
		}else{
			$json = array (
				'message' 	=> 'List Laporan kosong',
				'data'		=> '',
				'code'		=> 500
			);
		}
		
		$this->outputJson($json);
	}

	public function takenLaporan(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		$data = $this->laporan_model->takenlaporan($postdata);
		$id_pelapor = $this->laporan_model->getlaporanByid($postdata['id_laporan']);
		//log_message('info','id_laporan '.$postdata['id_laporan']);
		$device_token = $this->pelapor_model->getPelaporById($id_pelapor[0]->id_pelapor);
		//log_message('info','device_token '.$device_token[0]->device_token);

		foreach ($device_token as $device_token) {
			$send = $this->send_push_notif("Laporan sedang ditangani ",$device_token->device_token);
		}

		
		$json = array (
			'message' 	=> 'Sukses menangani laporan',
			'data'		=> '',
			'code'		=> 200
		);
		
		$this->outputJson($json);
	}

	public function invitation(){
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		$email = $this->input->post('email');
		$type_akun = $this->input->post('type_akun'); //1. pelapor 2.solver
		$id_department = (!empty($this->input->post('id_department')) ? $this->input->post('id_department'): 0);
		$id_organisasi = $this->input->post('id_organisasi');
		
		//cek email already use or not. If has been used just input and notif to email
		$data_pelapor = $this->pelapor_model->getDataPelaporByEmail($email);
		$data_admin = $this->admin_model->getDataAdmin($email);
		$message ='';
		if(!empty($data_pelapor)){
			if($type_akun==1){ // as pelapor
				$status_pelapor_in_organisasi = $this->pelapor_model->getDataPelaporByIdOrganisasi($data_pelapor[0]->id,$id_organisasi);
				if(!empty($status_pelapor_in_organisasi)){
					$json=array(
						'message' 	=> 'Email telah digunakan sebagai pelapor pada organisasi yang anda pimpin',
						'data'		=> '',
						'code'		=> 404
					);
				}else{
					$data_oganisasi['id_organisasi'] = $id_organisasi;
					$data_oganisasi['id_pelapor'] = $data_pelapor[0]->id;
					$this->pelapor_model->add_pelapor_organisasi($data_oganisasi);
					$json=array(
						'message' 	=> 'Sukses mengirim undangan',
						'data'		=> '',
						'code'		=> 200
					);
					$data_email['name_group'] = "Pelapor";
					$message = $this->load->view('template-email-invitation',$data_email,TRUE);
					//sent email
				}
			}else{ // as solver
				if(!empty($data_admin)){
					$staus_admin_in_organisasi = $this->admin_model->getDataAdminByIdAndOrganisasi($data_admin[0]->id,$id_organisasi);
					if(!empty($staus_admin_in_organisasi)){
						$json=array(
							'message' 	=> 'Email telah digunakan sebagai solver pada organisasi yang anda pimpin',
							'data'		=> '',
							'code'		=> 404
						);
					}else{
						$data_oganisasi['id_organisasi'] = $id_organisasi;
						$data_oganisasi['id_admin'] = $data_admin[0]->id;
						$data_oganisasi['id_department'] = $id_department;
						$this->admin_model->add_admin_organisasi($data_oganisasi);
						$json=array(
							'message' 	=> 'Sukses mengirim undangan',
							'data'		=> '',
							'code'		=> 200
						);
						$data_email['name_group'] = "Solver";
						$message = $this->load->view('template-email-invitation',$data_email,TRUE);
					}
				}else{
					//insert sebagai admin
					$data_admin['username'] = $data_pelapor[0]->username;
					$data_admin['no_telphone'] = $data_pelapor[0]->no_telphone;
					$data_admin['email'] = $email;
					$data_admin['password'] = $data_pelapor[0]->password;
					$data_admin['device_token'] = $data_pelapor[0]->device_token;
					$data_admin['status_delete'] = true;
					$data_admin['status_active'] = true;
					$id_admin = $this->admin_model->add($data_admin);

					$data_oganisasi['id_organisasi'] = $id_organisasi;
					$data_oganisasi['id_admin'] = $id_admin;
					$data_oganisasi['id_department'] = $id_department;
					$data_oganisasi['id_admin_group'] = 1;

					$this->admin_model->add_admin_organisasi($data_oganisasi);
					$json=array(
						'message' 	=> 'Sukses mengirim undangan',
						'data'		=> '',
						'code'		=> 200
					);

					//sent email
					$data_email['name_group'] = "Solver";
					$message = $this->load->view('template-email-invitation',$data_email,TRUE);
				}
			}
		}elseif (!empty($data_admin)) {
			if($type_akun==2){ // as solver
				$staus_admin_in_organisasi = $this->admin_model->getDataAdminByIdAndOrganisasi($data_admin[0]->id,$id_organisasi);
				if(!empty($staus_admin_in_organisasi)){
					$json=array(
						'message' 	=> 'Email telah digunakan sebagai solver',
						'data'		=> '',
						'code'		=> 404
					);
				}else{
					$data_oganisasi['id_organisasi'] = $id_organisasi;
					$data_oganisasi['id_admin'] = $data_admin[0]->id;
					$data_oganisasi['id_department'] = $id_department;
					$this->admin_model->add_admin_organisasi($data_oganisasi);
					$json=array(
						'message' 	=> 'Sukses mengirim undangan',
						'data'		=> '',
						'code'		=> 200
					);

					//sent email
					$data_email['name_group'] = "Solver";
					$message = $this->load->view('template-email-invitation',$data_email,TRUE);
				}
			}else{ // as pelapor
				if(!empty($data_pelapor)){
					$staus_admin_in_organisasi = $this->admin_model->getDataAdminByIdAndOrganisasi($data_admin[0]->id,$id_organisasi);
					if(!empty($staus_admin_in_organisasi)){
						$json=array(
							'message' 	=> 'Email telah digunakan sebagai solver pada organisasi yang anda pimpin',
							'data'		=> '',
							'code'		=> 404
						);
					}else{
						$data_oganisasi['id_organisasi'] = $id_organisasi;
						$data_oganisasi['id_admin'] = $data_admin[0]->id;
						$data_oganisasi['id_department'] = $id_department;
						$this->admin_model->add_admin_organisasi($data_oganisasi);
						$json=array(
							'message' 	=> 'Sukses mengirim undangan',
							'data'		=> '',
							'code'		=> 200
						);

						//sent email
						$data_email['name_group'] = "Solver";
						$message = $this->load->view('template-email-invitation',$data_email,TRUE);
					}
				}else{
					//insert sebagai admin
					$data_admin['username'] = $data_admin[0]->username;
					$data_admin['no_telphone'] = $data_admin[0]->no_telphone;
					$data_admin['email'] = $email;
					$data_admin['password'] = $data_admin[0]->password;
					$data_admin['device_token'] = $data_admin[0]->device_token;
					$data_admin['status_delete'] = true;
					$data_admin['status_active'] = true;
					$id_pelapor = $this->pelapor_model->add($data_admin);

					$data_oganisasi['id_organisasi'] = $id_organisasi;
					$data_oganisasi['id_pelapor'] = $id_pelapor;
					$this->pelapor_model->add_pelapor_organisasi($data_oganisasi);

					$json=array(
						'message' 	=> 'Sukses mengirim undangan',
						'data'		=> '',
						'code'		=> 200
					);

					//sent email
					$data_email['name_group'] = "Pelapor";
					$message = $this->load->view('template-email-invitation',$data_email,TRUE);
				}
			}
		}else{
			if($type_akun==1){
				$data['email'] = $email;
				$data['status_delete'] = true;
				$data['status_active'] = false;

				$id_pelapor = $this->pelapor_model->add($data);
				$data_oganisasi['id_organisasi'] = $id_organisasi;
				$data_oganisasi['id_pelapor'] = $id_pelapor;
				$this->pelapor_model->add_pelapor_organisasi($data_oganisasi);

				$data_email['name_group'] = "Pelapor";
				$data_email['id_pelapor'] = $id_pelapor;
				$data_email['id_organisasi'] = $id_organisasi;

				$message = $this->load->view('template-email-invitation',$data_email,TRUE);
				
				$json=array(
						'message' 	=> 'Sukses mengirim undangan',
						'data'		=> '',
						'code'		=> 200
				);
			}else{
				log_message("error","masuk else sebagai solver");
				$data['email'] = $email;
				$data['status'] = true;
				$data['status_active'] = false;

				$id_pelapor = $this->admin_model->add($data);

				$data_oganisasi['id_organisasi'] = $id_organisasi;
				$data_oganisasi['id_admin'] = $id_pelapor;
				$data_oganisasi['id_department'] = $id_department;
				$data_oganisasi['id_admin_group'] = 1;
				$this->admin_model->add_admin_organisasi($data_oganisasi);

				$data_email['name_group'] = "Solver";
				$data_email['id_pelapor'] = $id_pelapor;
				$data_email['id_organisasi'] = $id_organisasi;
				$data_email['id_department'] = $id_department;


				$message = $this->load->view('template-email-invitation',$data_email,TRUE);

				/*$message ="<b>Selamat Datang, </b><br>";
				$message .= "<p>Anda mendapat undangan sebagai <b> solver<b>.";
				$message .= 'Silahkan lengkapi data diri anda untuk login pada aplikasi, silahkan kunjungi tautan berikut </p>'; 
				$message .= "<br><a href='".base_url('welcome/sign_up_invitation/'.base64_encode($id_pelapor).'/'.base64_encode($id_organisasi).'/'.base64_encode($id_department))."'>".base_url('welcome/sign_up_invitation/'.base64_encode($id_pelapor).'/'.base64_encode($id_organisasi).'/'.base64_encode($id_department))."</a>";*/
				$json=array(
					'message' 	=> 'Sukses mengirim undangan',
					'data'		=> '',
					'code'		=> 200
				);
			}
		}
		$this->email->from('info@adhmora.com', "Admin Slick");
		$this->email->to($email);

		$this->email->subject('Invitation');
		
		$this->email->message($message);
		$this->email->set_mailtype('html');
		if (!$this->email->send()) {
             log_message('error',$this->email->print_debugger());
        } 
		
		$this->outputJson($json);
	}

	public function get_role(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		@$email = $postdata['email'];
		@$id_organisasi = $postdata['id_organisasi'];

		$data = $this->admin_model->get_role($email,$id_organisasi);
		$json = array (
			'message' 	=> 'Sukses mengambil role',
			'data'		=> $data,
			'code'		=> 200
		);
		
		$this->outputJson($json);
		
	}

	private function outputJson($response=array(),$status=200){
		$this->output
		->set_status_header($status)
		->set_content_type('application/json', 'utf-8')
		->set_output(json_encode($response, JSON_PRETTY_PRINT))
		->_display();
		exit();
	}

	function send_push_notif($message,$device_token){
		log_message('info','siap kirim laporan ');
		// prep the bundle
		$msg = array
		(
			'message' 	=> $message,
			'title'		=> 'Application Slick',
			'subtitle'	=> 'Aplikasi Slick',
			'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
			'vibrate'	=> 1,
			// "sound"		=> "beep", // custom sound .\platforms\android\res\raw
			"sound"		=> "default",
    		"badge"		=> "1",
			'largeIcon'	=> 'large_icon',
			'smallIcon'	=> 'small_icon'
		);
		log_message('info','device tokem '. $device_token);
		$fields = array
		(
			'registration_ids' 	=> array($device_token),
			'data'			=> $msg
		);
		 
		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		 
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		log_message('info','beres kriim');
	}

}
