<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


	function __construct(){
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');
		parent::__construct();
		$this->load->library('image_lib');
		$this->load->model(array('admin_model','laporan_model','pelapor_model'));

	}

	public function index(){
		$this->load->view('welcome_message');
	}

	public function uploadFile(){
		$location = trim($_POST['folder']);

		$uploadfile = $_POST['namaFile'];
		$uploadfilename = $_FILES['file']['tmp_name'];

		if(move_uploaded_file($uploadfilename, './'.$location.'/'.$uploadfile)){
			$config['image_library'] = 'gd2';
			$config['source_image']  = './uploads/'.$uploadfile;
			$config['new_image']  = './uploads/'.$uploadfile;
			$config['maintain_ratio'] = TRUE;
			$config['width']	 =600;
			$config['height']	= 600;

			$this->image_lib->initialize($config); 


			$this->image_lib->resize();
			echo 'berhasil';
		} else {
			echo 'gagal';
		}
	}
	
	public function sign_up(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		
		$data = array();
		$data['email']=$postdata['email'];
		$data['no_telphone']=$postdata['no_telphone'];
		$data['username']=$postdata['username'];
		$data['password']=md5($postdata['password']);
		$data['id_admin_group']=$postdata['id_admin_group'];
		$data['id_department']=$postdata['id_department'];

		if(!$this->admin_model->cekUsername($data['username'])){
			if(!$this->admin_model->cekEmail($data['email'])){
				if(!$this->admin_model->cekPhone($data['no_telphone'])){
					if($this->admin_model->add($data)){
						//berhasil infut
						$json = array (
							'message' 	=> 'Akun berhasil di daftarkan.',
							'data'		=> '',
							'code'		=> 200

						);
					}else{ //gagal input
						$json = array (
							'message' 	=> 'Maaf anda gagal mendaftar, silahkan coba lagi ',
							'data'		=> '',
							'code'		=> 500

						);
					}
				}else{ //no telphone sudah terdaftar
					$json = array (
							'message' 	=> 'Maaf nomor telphone sudah digunakan',
							'data'		=> '',
							'code'		=> 500

					);
				}
			}else{ //email sudah terdaftar
					$json = array (
							'message' 	=> 'Maaf email sudah digunakan',
							'data'		=> '',
							'code'		=> 500

					);
			}
		}else{ //email sudah terdaftar
					$json = array (
							'message' 	=> 'Maaf username sudah digunakan',
							'data'		=> '',
							'code'		=> 500

					);
		}

		$this->outputJson($json);
	}

	public function login(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		$data = array();
		$data['device_token']=$postdata ['device_token'];
		$data['email']=$postdata ['email'];
		$data['password']=md5($postdata ['password']);

		$status = $this->admin_model->cekLogin($data)->result();
		if($status!=null){
			//update device token
			$this->admin_model->updateDeviceToken($data['device_token'],$status[0]->id);
			$json = array (
						'message' 	=> 'Login Berhasil',
						'data'		=> $status,
						'code'		=> 200

			);
		}else{
			$json = array (
						'message' 	=> 'Maaf email atau password tidak sama',
						'data'		=> '',
						'code'		=> 500

			);
		}

		
		$this->outputJson($json);
	}

	public function reportProgress(){
		
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		if(!empty($postdata)){
			$gambar = $postdata['foto'];
			$update = $this->laporan_model->updateProggress($postdata,$gambar);


			if($update){
				//siap untuk report proggres ke pelapor
				if($postdata['progress']==100){
					$id_pelapor = $this->laporan_model->getlaporanByid($postdata['id_laporan']);
					$device_token = $this->pelapor_model->getPelaporById($id_pelapor[0]->id_pelapor);
					$this->send_push_notif("Laporan telah selesai ditangani",$device_token[0]->device_token);

				}
				$json = array (
					'message' 	=> 'Berhasil Lapor Progress',
					'data'		=> '',
					'code'		=> 200
				);
			}else{
				$json = array (
					'message' 	=> 'Gagal melaporkan',
					'data'		=> '',
					'code'		=> 500
				);
			}			
		}
        
		$this->outputJson($json);
	}

	public function tes($id){
		$admin = $this->admin_model->GetAdminByDepartment($id);
			foreach ($admin as $admin) {
				echo $admin->device_token;
			}
	}

	public function updatePassword(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		$data = array();
		$data['password'] = md5($postdata['password']);
		$data['id'] = $postdata['id'];

		if($this->admin_model->updatePassword($data)){
			$json = array (
						'message' 	=> 'Password berhasil diubah',
						'data'		=> '',
						'code'		=> 200

			);
		}else{
			$json = array (
						'message' 	=> 'Password gagal diubah',
						'data'		=> '',
						'code'		=> 500

			);
		}
		
		$this->outputJson($json);
	}

	public function listLaporan(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		@$offset = ($postdata['offset']!=null) ? $postdata['offset'] : 0;
		@$limit = ($postdata['limit']!=null) ? $postdata['limit'] : 10;

		$data = $this->laporan_model->getLaporan($postdata,$limit,$offset);

		if(!empty($data)){
			$json = array (
				'message' 	=> 'List Laporan berhasil di ambil',
				'data'		=> $data,
				'code'		=> 200
			);
		}else{
			$json = array (
				'message' 	=> 'List Laporan kosong',
				'data'		=> '',
				'code'		=> 500
			);
		}
		
		$this->outputJson($json);
	}

	public function takenLaporan(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		$data = $this->laporan_model->takenlaporan($postdata);
		$id_pelapor = $this->laporan_model->getlaporanByid($postdata['id_laporan']);

		$device_token = $this->pelapor_model->getPelaporById($id_pelapor[0]->id_pelapor);

		$send = $this->send_push_notif("Laporan sedang ditangani ",$device_token[0]->device_token);

		$json = array (
			'message' 	=> 'Sukses menangani laporan',
			'data'		=> '',
			'code'		=> 200
		);
		
		$this->outputJson($json);
	}

	private function outputJson($response=array(),$status=200){
		$this->output
		->set_status_header($status)
		->set_content_type('application/json', 'utf-8')
		->set_output(json_encode($response, JSON_PRETTY_PRINT))
		->_display();
		exit();
	}

	function send_push_notif($message,$device_token){

		// prep the bundle
		$msg = array
		(
			'id'		=> 1,
			'message' 	=> $message,
			'title'		=> 'Application Slick',
			'subtitle'	=> 'Aplikasi Slick',
			'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
			'vibrate'	=> 1,
			'sound'		=> 1,
			'largeIcon'	=> 'large_icon',
			'smallIcon'	=> 'small_icon'
		);
		$fields = array
		(
			'registration_ids' 	=> array($device_token),
			'data'			=> $msg
		);
		 
		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		 
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
	}

}
