<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	
	function __construct(){
		/*header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');*/
		parent::__construct();

		$this->load->model('laporan_model');
	}

	public function index(){
		$this->load->view('welcome_message');
	}

	public function lapor(){
		
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		 //set preferences
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'png|jpg|jpeg';

        //load upload class library
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('foto_pelapor')) {
        	$error = array('error' => $this->upload->display_errors()); 
        	print_r($error);
        	$json = array (
						'message' 	=> 'Gagal melaporkan',
						'data'		=> '',
						'code'		=> 500

			);
        }else{
        	$gambar = $this->upload->data('file_name');
        	$this->laporan_model->add($postdata,$gambar);
        	$json = array (
						'message' 	=> 'Berhasil Melaporkan',
						'data'		=> '',
						'code'		=> 200

			);
        }

        $this->outputJson($json);
	}

	public function detailLaporan(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		$data = $this->laporan_model->getlaporanByid($postdata['id']);
		if(!empty($data)){
			$json = array (
						'message' 	=> 'Detail Laporan berhasil di ambil',
						'data'		=> $data,
						'code'		=> 200

			);
		}else{
			$json = array (
						'message' 	=> 'Detail Laporan kosong',
						'data'		=> '',
						'code'		=> 500
			);
		}
		$this->outputJson($json);
	}

	public function getLaporan(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		$data = $this->laporan_model->getLaporan($postdata);
		if(!empty($data)){
			$json = array (
						'message' 	=> 'List Laporan berhasil di ambil',
						'data'		=> $data,
						'code'		=> 200

			);
		}else{
			$json = array (
						'message' 	=> 'List Laporan gagal di ambil',
						'data'		=> '',
						'code'		=> 500

			);
		}
		$this->outputJson($json);
	}

	private function outputJson($response=array(),$status=200){
		$this->output
		->set_status_header($status)
		->set_content_type('application/json', 'utf-8')
		->set_output(json_encode($response, JSON_PRETTY_PRINT))
		->_display();
		exit();
	}
}
