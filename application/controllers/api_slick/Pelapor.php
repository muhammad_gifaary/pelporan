<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelapor extends CI_Controller {

	
	function __construct(){
		parent::__construct();
		// Add origin header 
		/*header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');*/
		$this->load->library('image_lib');
		$this->load->model('pelapor_model');
		$this->load->model('admin_model');
		//$this->load->library('email');
		//$this->config->load('email');
		
	}

	private function outputJson($response=array(),$status=200){
		$this->output
		->set_status_header($status)
		->set_content_type('application/json', 'utf-8')
		->set_output(json_encode($response, JSON_PRETTY_PRINT))
		->_display();
		exit();
	}

	public function tambah_departement(){
		$this->load->model('department_model');
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		@$nama = $this->input->post('nama');
		@$id_organisasi = $this->input->get('id_organisasi');

		$json = array (
			'message' 	=> 'Departemen berhasil ditambahkan',
			'data'		=> $this->department_model->add(array('name' =>$nama,'id_organisasi'=>$id_organisasi)),
			'code'		=> 200
		);

		$this->outputJson($json);
	}

	public function uploadFile(){
		$location = trim($_POST['folder']);

		$uploadfile = $_POST['namaFile'];
		$uploadfilename = $_FILES['file']['tmp_name'];

		if(move_uploaded_file($uploadfilename, './'.$location.'/'.$uploadfile)){
			 	$config['image_library'] = 'gd2';
				$config['source_image']  = './uploads/'.$uploadfile;
				$config['new_image']  = './uploads/'.$uploadfile;
				$config['maintain_ratio'] = TRUE;
				$config['width']	 =400;
				$config['height']	= 400;

				$this->image_lib->initialize($config); 
				$this->image_lib->resize();

			echo true;
		} else {
			echo false;
		}
	}
	
	public function index(){
		$json=array(
			'message' 	=> 'Naon?',
			'data'		=> '',
			'code'		=> 404
		);
		//$this->outputJson($json);
	}

	public function sign_up(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		@$email = $postdata['email'];
		@$no_telphone = $postdata['no_telphone'];
		@$username = $postdata['username'];
		@$password = $postdata['password'];
		@$id_organisasi = $postdata['id_organisasi'];
		if(!empty($email)){
			// validasi
			$cekUsername = $this->pelapor_model->cekUsername($username);
			$cekEmail = $this->pelapor_model->cekEmail($email);
			$cekPhone = $this->pelapor_model->cekPhone($no_telphone);

			if(!$cekUsername){
				if(!$cekEmail){
					if(!$cekPhone){
						$data = array(
							'email' => $email,
							'no_telphone' => $no_telphone,
							'username'	=> $username,
							'password' => md5($password),
							'status'	=> 'f',
							'device_token' => '',
							'id_organisasi' => $id_organisasi
						);
						// insert data user
						$insert = $this->pelapor_model->add($data);

						//konfigurasi email
						
						$this->email->from('info@adhmora.com', "Admin Slick");
						$this->email->to($email);

						$this->email->subject('Aktivasi Akun');
						$message ="<b>Selamat Datang, </b><br>";
						$message = '<p>Silahkan klik link berikut untuk mengaktifkan akun <a href="'.site_url("/pelapor/active_account/".(base64_encode($insert))).'">Aktifkan Akun</a></p>';
						$this->email->message($message);
						$this->email->set_mailtype('html');

						if (!$this->email->send()) {
				             log_message('error',$this->email->print_debugger());
				        } 
						if($insert){
							$json = array (
								'message' 	=> 'Silahkan cek email untuk aktivasi akun',
								'data'		=> '',
								'code'		=> 200
							);
						}else{
							$json = array (
								'message' 	=> 'Maaf anda gagal mendaftar, silahkan coba lagi ',
								'data'		=> '',
								'code'		=> 500
							);
						}
					}else{
						$json = array (
							'message' 	=> 'Maaf nomor telphone sudah digunakan',
							'data'		=> '',
							'code'		=> 500
						);
					}
				}else{
					$json = array (
						'message' 	=> 'Maaf email sudah digunakan',
						'data'		=> '',
						'code'		=> 500
					);
				}
			}else{
				$json = array (
					'message' 	=> 'Maaf username sudah digunakan',
					'data'		=> '',
					'code'		=> 500
				);
			}
		}

		$this->outputJson($json);
	}

	public function login(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		@$email = $postdata['email'];
		@$password = $postdata['password'];
		@$device_token = $postdata['device_token'];

		$data = array();
		$data['device_token']=$device_token;
		$data['email']=$email;
		$data['password']=md5($password);
		if(!empty($email)){

			$active_admin = $this->admin_model->cek_active($data['email']);
			$string_token =  "Adhm0r4Sl1cKHubT3rB4rUDevM9R9";
			$_token =substr(str_shuffle($string_token), 0,16);

			if($active_admin!=null){
				//cek apakah sudah di aktivasia atau belum
				if($active_admin==false){
					$json = array (
								'message' 	=> 'Akun belum diaktifkan',
								'data'		=> '',
								'code'		=> 500
					);
				}else{
					$status = $this->admin_model->cekLogin($data)->result();
					if($status!=null){
						//update device token
						
						$this->pelapor_model->updateDeviceToken($data['device_token'],$email,$_token);
						$this->admin_model->updateDeviceToken($data['device_token'],$email,$_token);
						$json = array (
									'message' 	=> 'Login Berhasil',
									'data'		=> $status,
									'code'		=> 200

						);
					}else{
						$json = array (
									'message' 	=> 'Email atau Password tidak sama',
									'data'		=> '',
									'code'		=> 500

						);
					}
				}
			}else{ //jika tidak terdaftar sebagai admin atau solver cek sebagai pelapor
				$status = $this->pelapor_model->cekLogin($data)->result();
				if($status!=null){
					//update device token
					$this->pelapor_model->updateDeviceToken($data['device_token'],$email,$_token);
					$this->admin_model->updateDeviceToken($data['device_token'],$email,$_token);
					
					$json = array (
						'message' 	=> 'Login Pelapor Berhasil',
						'data'		=> $status,
						'code'		=> 200
					);
				}else{
					$json = array (
							'message' 	=> 'Email atau Password tidak sama',
							'data'		=> '',
							'code'		=> 500
					);	
				}
			}
		}

		$this->outputJson($json);
	}

	public function logout(){
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		$email = $this->input->post('email');
		$this->pelapor_model->logout($email);
		$this->admin_model->logout($email);
		$json = array (
					'message' 	=> 'logout berhasil',
					'data'		=> '',
					'code'		=> 200
		);
		$this->outputJson($json);
	}

	public function lapor(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		@$status_urgent = $postdata['status_urgent'];
		@$lokasi = $postdata['lokasi'];
		@$id_department = $postdata['id_department'];
		@$id_pelapor = $postdata['id_pelapor'];
		@$foto_pelapor1 = $postdata['foto_pelapor1'];
		@$foto_pelapor2 = $postdata['foto_pelapor2'];
		@$foto_pelapor3 = $postdata['foto_pelapor3'];
		@$id_organisasi = $postdata['id_organisasi'];
		// echo $foto_pelapor;exit();

		if(!empty($foto_pelapor1)){
			$data = array();
			$data['status_urgent']=$status_urgent;
			$data['lokasi']=$lokasi;
			$data['id_department']=$id_department;
			$data['id_pelapor']=$id_pelapor;
			$data['foto_pelapor']=$foto_pelapor1;
			$data['foto_pelapor2']=$foto_pelapor2;
			$data['foto_pelapor3']=$foto_pelapor3;
			$data['notes_pelapor']=(!empty($postdata['notes_pelapor'])) ? $postdata['notes_pelapor'] : '';
			$add = $this->pelapor_model->addLaporan($data);
			
			if($add){
				$json = array (
					'message' 	=> 'Berhasil Melaporkan',
					'data'		=> '',
					'code'		=> 200
				);
				$this->load->model('admin_model');
				$admin = $this->admin_model->GetAdminByDepartmentAndOrganisasi($id_department,$id_organisasi);
				if(!empty($admin)){
					foreach ($admin as $admin) {
						$this->send_push_notif("Terdapat laporan baru ",$admin->device_token);
					}
				}
			}else{
				$json = array (
					'message' 	=> 'Gagal melaporkan',
					'data'		=> '',
					'code'		=> 500
				);
			}
		}

		$this->outputJson($json);
	}

	public function active_account($pelapor_id){
		$id = base64_decode($pelapor_id);
		$this->pelapor_model->updateStatusActive($id);
		echo "<font size='6'><b>Akun telah diaktifkan, mohon tunggu aktivasi dari Admin organisasi anda, untuk login pada aplikasi</b></font>";
	}

	public function get_data_pelapor_by_email(){
		//$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		log_message("error","email".$this->input->post('email'));
		$data = $this->pelapor_model->getDataPelaporByEmail($this->input->post('email'));
		$json = array (
					'message' 	=> 'data berhasil di ambil',
					'data'		=> $data,
					'code'		=> 200
		);
		$this->outputJson($json);
	}
	public function get_data_pelapor(){
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		$data = $this->pelapor_model->getDataPelapor((int)$this->input->post("id"));
		$json = array (
					'message' 	=> 'data berhasil di ambil',
					'data'		=> $data,
					'code'		=> 200
		);
		$this->outputJson($json);
	}

	public function activeAccount(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		@$id = $postdata['id'];

		if(!empty($id)){
			if($this->pelapor_model->update_status_by_admin_user($id)){
				$email = $this->pelapor_model->getDataPelapor($id);
				$config['protocol']='IMAP';  

				$config['smtp_host']='mail.adhmora.com';  

				$config['smtp_port']='143';

				$config['smtp_timeout']='150';    

				$config['smtp_user']='info@adhmora.com';  

				$config['smtp_pass']='1q2w3e4r';  

				$config['charset']='utf-8';

				$config['newline']="\r\n";  

				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('info@adhmora.com', "Admin Slick");
				$this->email->to($email[0]->email);

				$this->email->subject('Aktivasi Akun');
				$message ="<b>Selamat Datang, </b><br>";
				$message = '<p>Akun anda telah diaktifkan oleh Admin organisasi anda, anda dapat login pada aplikasi</p>';
				$this->email->message($message);
				$this->email->set_mailtype('html');

				if (!$this->email->send()) {
		             log_message('error',$this->email->print_debugger());
		        } 
				$json = array (
					'message' 	=> 'Akun telah diaktifkan',
					'data'		=> '',
					'code'		=> 200
				);
			}else{
				$json = array (
					'message' 	=> 'Akun gagal diaktifkan',
					'data'		=> '',
					'code'		=> 500
				);
			}
		}

		$this->outputJson($json);
	}

	public function edit_profile(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		@$no_telphone = $postdata['no_telphone'];
		@$username = $postdata['username'];
		@$email = $postdata['email'];

		// validasi
		$cekUsername = $this->pelapor_model->cekUsername($username);
		$cekPhone = $this->pelapor_model->cekPhone($no_telphone);
		$data_lama = $this->pelapor_model->getDataPelaporByEmail($email);

		if(!$cekUsername || strcmp($username, $data_lama[0]->username)==0){
			if($cekPhone==false || strcmp($no_telphone, $data_lama[0]->no_telphone)==0){
				$data = array(
					'no_telphone' => $no_telphone,
					'username'	=> $username
				);
				// insert data user
				//$this->admin_model->update($data,$email);
				$this->pelapor_model->update($data,$email);
				
				$json = array (
					'message' 	=> 'Data pribadi berhasil diubah',
					'data'		=> '',
					'code'		=> 200
				);
			}else{
				$json = array (
					'message' 	=> 'Maaf nomor telphone sudah digunakan',
					'data'		=> '',
					'code'		=> 500
				);
			}
		}else{
			$json = array (
				'message' 	=> 'Maaf username sudah digunakan',
				'data'		=> '',
				'code'		=> 500
			);
		}

		$this->outputJson($json);
	}
	public function updatePassword(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		@$password = $postdata['password'];
		@$email = $postdata['email'];
		@$password_lama = $postdata['password_lama'];

		if(!empty($password)){

			if($this->pelapor_model->cekPassword(md5($password_lama))){
				$data = array();
				$data['password'] = md5($password);
				$data['email'] = $email;

				if($this->pelapor_model->updatePasswordByEmail($data)){
					$this->admin_model->updatePasswordByEmail($data);
					$json = array (
						'message' 	=> 'Password berhasil diubah',
						'data'		=> '',
						'code'		=> 200
					);
				}else{
					$json = array (
						'message' 	=> 'Password gagal diubah',
						'data'		=> '',
						'code'		=> 500
					);
				}
			}else{
				$json = array (
						'message' 	=> 'Password lama salah',
						'data'		=> '',
						'code'		=> 500
				);
			}
			
		}

		$this->outputJson($json);
	}

	public function getAllPelapor(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		@$offset = ($postdata['offset']!=null) ? $postdata['offset'] : 0;
		@$limit = ($postdata['limit']!=null) ? $postdata['limit'] : 10;
		$data = $this->pelapor_model->get_by_organisasi($offset,$limit,$postdata['id_organisasi']);
		if(!empty($data)){
			$json = array (
				'message' 	=> 'List pelapor berhasil di ambil',
				'data'		=> $data,
				'code'		=> 200
			);
		}else{
			$json = array (
				'message' 	=> 'List pelapor kosong',
				'data'		=> '',
				'code'		=> 500
			);
		}
		$this->outputJson($json);
	}

	public function tes(){
		$data = $this->pelapor_model->getlaporanByid(1,10,10);
		print_r($data);
	}

	public function getLaporanByPelapor(){
		//$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		log_message("error","id pelapor".$this->input->post("id"));
		@$offset = ($this->input->post("offset")!=null) ? $this->input->post("offset") : 0;
		@$limit = ($this->input->post("limit")!=null) ? $this->input->post("limit") : 0;
		@$id = (int)$this->input->post("id");

		$data = $this->pelapor_model->getlaporanByid($id,$limit,$offset);
		if(!empty($data)){
			$json = array (
				'message' 	=> 'List Laporan berhasil di ambil',
				'data'		=> $data,
				'code'		=> 200
			);
		}else{
			$json = array (
				'message' 	=> 'List Laporan kosong',
				'data'		=> '',
				'code'		=> 500
			);
		}
		
		$this->outputJson($json);
	}

	public function getOneLaporan(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		@$id = $postdata['id'];

		$data = $this->pelapor_model->getOneLaporan($id);

		if(!empty($data)){
			$json = array (
				'message' 	=> 'List Laporan berhasil di ambil',
				'data'		=> $data[0],
				'code'		=> 200
			);
		}else{
			$json = array (
				'message' 	=> 'List Laporan kosong',
				'data'		=> '',
				'code'		=> 500
			);
		}
		
		$this->outputJson($json);
	}

	public function reset_password(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);

		$config['protocol']='IMAP';  

		$config['smtp_host']='mail.adhmora.com';  

		$config['smtp_port']='143';

		$config['smtp_timeout']='150';  

		$config['smtp_user']='info@adhmora.com';  

		$config['smtp_pass']='1q2w3e4r';  

		$config['charset']='utf-8';

		$config['newline']="\r\n";  

		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);

		@$email = $postdata['email'];
		@$no_telphone = $postdata['no_telphone'];
		$cekEmail = $this->pelapor_model->cekEmail($email);
		$cekPhone = $this->pelapor_model->cekPhone($no_telphone);
		
		
		$char_password =str_shuffle("SlIcKApplikasibagus99");
		$password = substr($char_password,0,6);
		$data = array(
			'password' 	=> md5($password),
			'email'		=> $email
		);
		if($cekEmail && $cekPhone){
			$this->pelapor_model->updatePasswordByEmail($data);
			$this->admin_model->updatePasswordByEmail($data);

			$this->email->from('info@adhmora.com', "Admin Slick");
			$this->email->to($postdata['email']);

			$this->email->subject('Reset Password');
			$message = 'Silahkan login dengan password <b> '.$password."</b> dan ";
			$this->email->message($message);
			$this->email->set_mailtype('html');

			$this->email->send();

			$json = array (
				'message' 	=> 'Password baru telah dikirim ke email anda',
				'data'		=> '',
				'code'		=> 200
			);
		}else{
			$cekEmail = $this->admin_model->cekEmail($email);
			$cekPhone = $this->admin_model->cekPhone($no_telphone);
			if($cekEmail && $cekPhone){
				$this->pelapor_model->updatePasswordByEmail($data);
				$this->admin_model->updatePasswordByEmail($data);
				
				$this->email->from('info@adhmora.com', "Admin Slick");
				$this->email->to($email);

				$this->email->subject('Reset Password');
				$message = 'Silahkan login dengan password <b> '.$password."</b> dan ";
				$this->email->message($message);
				$this->email->set_mailtype('html');

				$this->email->send();

				$json = array (
					'message' 	=> 'Password telah dikirimkan melalui email',
					'data'		=> '',
					'code'		=> 200
				);
			}else{
				$json = array (
					'message' 	=> 'Maaf email dan no telphone tidak sama',
					'data'		=> '',
					'code'		=> 500
				);
			}
		}
		$this->outputJson($json);
	}


	function send_push_notif($message,$device_token){
		// prep the bundle
		$msg = array
		(
			'message' 	=> $message,
			'title'		=> 'Application Slick',
			'subtitle'	=> 'Aplikasi Slick',
			'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
			'vibrate'	=> 1,
			// "sound"		=> "beep", // custom sound .\platforms\android\res\raw
			"sound"		=> "default",
    		"badge"		=> "1",
			'largeIcon'	=> 'large_icon',
			'smallIcon'	=> 'small_icon'
		);
		$fields = array
		(
			'registration_ids' 	=> array($device_token),
			'data'			=> $msg
		);
		 
		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		 
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
	}

	public function delete_pelapor(){
		$postdata = (array)json_decode(file_get_contents('php://input'));
		$json=array(
			'message' 	=> '',
			'data'		=> '',
			'code'		=> 404
		);
		
		$email= $this->pelapor_model->getDataPelapor($postdata['id']);

		$this->email->initialize($config);
		$this->email->from('info@adhmora.com', "Admin Slick");
		$this->email->to($email[0]->email);

		$this->email->subject('Hapus Akun');
		$message = '<p>Akun anda telah dihapus oleh Admin, silahkan daftar kembali</p>';
		$this->email->message($message);
		$this->email->set_mailtype('html');

		if (!$this->email->send()) {
             log_message('error',$this->email->print_debugger());
        } 
		$status = $data = $this->pelapor_model->delete_pelapor($postdata['id']);
		if($status){
			$json = array (
					'message' 	=> 'Akun berhasil dihapus',
					'data'		=> $data,
					'code'		=> 200
			);
		}else{
			$json = array (
					'message' 	=> 'Data gagal dihapus',
					'data'		=> $data,
					'code'		=> 500
			);
		}
		$this->outputJson($json);
	}

}