<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Organisasi_model extends CI_Model {

		private $table = 'organisasi';
		
		function __construct(){
			parent::__construct();
		}

		public function getAll(){

			$this->db->select("*");
			$this->db->from($this->table);
			$this->db->where('status', true);
			return $this->db->get()->result();
		}

		public function get_by_id($id){

			$this->db->select("*");
			$this->db->from($this->table);
			$this->db->where('status', true);
			$this->db->where('id', $id);
			return $this->db->get()->result();
		}

		public function add($data){
			$this->db->insert($this->table,$data);
			return $this->db->insert_id();
		}

		public function update($nama,$id){
			$this->db->set('nama', $nama);
			$this->db->where('id', $id);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;	
		}

		public function delete($id){
			$this->db->set('status', false);
			$this->db->where('id', $id);
			$this->db->update($this->table); 
			return ($this->db->affected_rows() != 1) ? false : true;
		}

	}
?>